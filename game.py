from random import randint

#player_name = prompt the player for their name
name = input("Hi! What is your name?\n")

# guess 1

for guess_number in range(1, 6):
# month_number = generate a random number
    month_number = randint(1,12)
# year_number = generate a random number
    year_number = randint(1924,2004)

# print this message:
   # "Guess " + guess_number + " : " + name + " were you" +
   # "born on " + month_number + " / " + year_number + " ?"
# response = prompt the player with "yes or no?"
    print("Guess 1 :", name, "were you born in", 
        month_number, "/", year_number, "?")

    response = input("yes or no?\n")
# if response is "yes", then
   # print the message "I knew it!"
# otherwise,
    #print the message "Drat! Lemme try again!"

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5: 
        print("I have other things to do. Good bye.")
    else: 
        print("Drat! Lemme try again!")


